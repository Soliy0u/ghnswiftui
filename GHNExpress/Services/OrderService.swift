//
//  OrderService.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import Combine

class OrderService: ApiService {
    static var instance: OrderService? = nil;
    
    /// Get instance
    static func getInstance() -> OrderService {
        if (instance == nil) {
            instance = OrderService()
            instance!.url = Server.stg.replacingOccurrences(of: PrefixDomain.prefix, with: PrefixDomain.shipping)
        }
        return instance!
    }
    static let shippingDetailByClientCode = "shipping-order/detail-by-client-code" // Order Info by Client_Order_Code
    
}

