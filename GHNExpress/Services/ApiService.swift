//
//  ApiService.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import Alamofire

class ApiService : NSObject{
    
    var parameters = Parameters()
    var headers = HTTPHeaders()
    var method: HTTPMethod!
    var url: String! = "https://abc"
    var encoding: ParameterEncoding! = JSONEncoding.default
    
    /// Init header
    func initHeader () {
        self.headers.update(name: "Content-Type", value: "application/json")
        let accessToken = UserDefaults.standard.string(forKey: StorageValueType.accessToken)
        self.headers.update(name: "Authorization", value: "Bearer \(accessToken ?? "")")
    }
    
    /// Init encode
    func initEncoding(_ method: HTTPMethod) {
        encoding = method == .post ? JSONEncoding.default : URLEncoding.default
    }
    
    override init(){
        super.init()
    }
    
    /// Execute query
    func executeQuery<T>(path:String = "", method: HTTPMethod = .post, parameters: Parameters?, completion: @escaping (Result<T, Error>) -> Void) where T: Codable {
        self.initHeader()
        self.initEncoding(method)
        AF.request(!path.contains("http") ? self.url + path : path, method: method, parameters: parameters, encoding: encoding, headers: headers).responseData(completionHandler: {response in
            switch response.result{
            case .success(let res):
                if let code = response.response?.statusCode{
                    switch code {
                    case 200...299:
                        do {
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            let value = try decoder.decode(T.self, from: res)
                            print(value)
                            completion(.success(value))
                        } catch let error {
                            print(String(data: res, encoding: .utf8) ?? "nothing received")
                            completion(.failure(error))
                        }
                    default:
                        let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
