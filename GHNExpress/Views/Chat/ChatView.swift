//
//  ChatView.swift
//  GHNExpress
//
//  Created by MacBook on 5/31/22.
//

import SwiftUI

struct ChatView: View {
    var body: some View {
        WRNavigationBar {
            VStack(spacing: 0) {
                EmptyFunctionCustom()
                Spacer()
            }
        }
        .backButtonHidden(false)
//        .backButtonImageName(Image.icBack)
        .wrNavigationBarItems(leading: {
            Text("Chat với CSKH").modifier(TextModifierCustom(fontType: .bold, fontSize: .medium)).foregroundColor(Color.mainText)
        }, trailing: {
//            Image.icNotifiActive.foregroundColor(Color.mainIcon)
        })
    }
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        ChatView()
    }
}
