//
//  LoginView.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import SwiftUI
import Focuser

struct LoginView: View {
    @StateObject var loginViewModel = LoginViewModel() // Login view model]
    @FocusStateLegacy var focusedField: LoginFormType? // Login form type

    
    var body: some View {
        VStack(alignment:.center){
            ScrollView(showsIndicators: false) {
                Image.icGhnLogo.resizable()
    //                    .aspectRatio(contentMode: .fill)
                    .frame(width:.infinity, height: 180)
    //                .padding(.padding48)
                VStack(alignment: .center){
                    VStack(alignment: .center){
                    Text("ĐĂNG NHẬP")
                        .modifier(TextModifierCustom(fontType: .bold, fontSize: .xlarge, fontColor: .mainText))
                        .padding(.bottom, .padding12)
                    Text("Chào ngày mới! \n Cùng chốt nhiều đơn hàng ngay hôm nay")
                            .multilineTextAlignment(.center)
                        .modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: .mainText))
                        .padding(.bottom, .padding24)
                    }
                    VStack(spacing: .margin16) {
                        // Username
                        CustomTextfieldTitle(inputTitle: "Số điện thoại/email", content: $loginViewModel.username, warningContent: $loginViewModel.warnUsername, rightTitle: "", placeHolder:"Vui lòng nhập số điện thoại hoặc email", isSecure: false, isFocused: .constant(self.focusedField == .username)).focusedLegacy($focusedField, equals: .username)
                                   
                        // Password
                        CustomTextfieldTitle(inputTitle: "Mật khẩu", content: $loginViewModel.password, warningContent: $loginViewModel.warnPassword, rightTitle: "Quên mật khẩu",  placeHolder:"Nhập mật khẩu của bạn", isSecure: true, isFocused: .constant(self.focusedField == .password)).focusedLegacy($focusedField, equals: .password)
                    }
                    
                    // Login btn
                    Button(action: {
    //                    loginViewModel.preAuth()
    //                    // Focus on view if have
//                        self.focusedField = loginViewModel.loginFormType
//                        if(self.focusedField == nil) {
//                            IQKeyboardManager.shared.resignFirstResponder()
//                        }
                    }){
                        Text("ĐĂNG NHẬP")
                            .modifier(TextModifierCustom(fontType: .main,  fontSize: .xmedium, fontColor: Color.white))
                            .modifier(SFButton())
                            .background(Color.gray)
                            .cornerRadius(.cornerRadius16)
                    }
                    .padding(.top, .padding16)
                    
                    HStack{
                        Text("Bạn đã có tài khoản chưa?")
                            .modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: .mainText))
                            .padding(.bottom, .padding24)
                            .padding(.top, .padding8)
                        Text("Đăng ký ngay")
                            .modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: Color.orange100))
                            .padding(.bottom, .padding24)
                            .padding(.top, .padding8)
                    }
                    
                    // Login btn
                    Button(action: {
                    }){
                        Text("Đăng nhập hỗ trợ khách hàng")
                            .modifier(TextModifierCustom(fontType: .main,  fontSize: .xmedium, fontColor: Color.orange100))
                            .modifier(WButton())
                            .frame(maxWidth: .none)
                            .background(Color.gray10)
                            .cornerRadius(.cornerRadius16)
                           .padding(0)
                    }
                    .padding(.top, .padding16)
                    VStack(alignment: .center){
                        Text("Dùng cho nhân sự GHN có thể đăng nhập bằng GHNID để hỗ trợ khách hàng")
                            .multilineTextAlignment(.center)
                            .modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: .mainText))
                            .padding(.bottom, .padding24)
                            .padding(.top, .padding8)
                    }
                }
            }
          
        }  .padding(.bottom, .padding48)
         .padding(.horizontal, .padding24)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
