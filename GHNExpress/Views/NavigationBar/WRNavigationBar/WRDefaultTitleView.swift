//
//  WRDefaultTitleView.swift
//  OMICall Contact Center
//
//  Created by MacBook on 11/23/21.
//

import Foundation
import SwiftUI

struct WRDefaultTitleView: View {
    let title: String
    let foreground: Color
    var body: some View {
        Text(title)
            .lineLimit(1)
            .foregroundColor(foreground)
            .font(.system(size: 17))
    }
}

struct WRDefaultTitleView_Previews: PreviewProvider {
    static var previews: some View {
        WRDefaultTitleView(title: "test", foreground: .black)
    }
}
