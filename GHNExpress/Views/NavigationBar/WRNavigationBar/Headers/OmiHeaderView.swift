//
//  OmiHeaderView.swift
//  OMICall Contact Center
//
//  Created by MacBook on 11/23/21.
//

import Foundation
import SwiftUI

public struct OmiHeaderView: View {
    let id = UUID()
    let systemIconName: String?
    let iconUrl: String?
    let iconSize: CGFloat?
    let iconVerticalPadding: CGFloat?
    let iconLeadingPadding: CGFloat?
    let iconTrailingPadding: CGFloat?
    let title: String
    let subTitle: String
    let labelFont: Font
    let foregrounColor: Color
    let foregroundInactiveColor: Color
    let callback: ((String) -> ())?
    let iconLeft: String
    let iconRight: String
    let iconRightCenter: String
    let iconDefault: String
    
    public init(
        systemIconName: String? = nil,
        iconSize: CGFloat? = nil,
        iconUrl: String? = "",
        iconLeft: String? = "",
        iconRight: String? = "",
        iconRightCenter: String? = "",
        iconDefault: String? = "",
        iconVerticalPadding: CGFloat? = nil,
        iconLeadingPadding: CGFloat? = nil,
        iconTrailingPadding: CGFloat? = nil,
        title: String,
        subTitle: String,
        labelFont: Font = Font.headline,
        foregrounColor: Color = Color.primary,
        foregroundInactiveColor: Color = Color.gray,
        callback: ((String) -> ())? = nil
    ) {
        self.systemIconName = systemIconName
        self.iconSize = iconSize
        self.iconUrl = iconUrl
        self.iconLeft = iconLeft ?? ""
        self.iconRight = iconRight ?? ""
        self.iconRightCenter = iconRightCenter ?? ""
        self.iconDefault = iconDefault ?? ""
        self.iconVerticalPadding = iconVerticalPadding
        self.iconLeadingPadding = iconLeadingPadding
        self.iconTrailingPadding = iconTrailingPadding
        self.title = title
        self.subTitle = subTitle
        self.labelFont = labelFont
        self.foregrounColor = foregrounColor
        self.foregroundInactiveColor = foregroundInactiveColor
        self.callback = callback
    }
    
    var iconLeftView: some View {
        Group {
            if let sfSymbolName = systemIconName {
                Image(systemName: sfSymbolName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width:iconSize ?? 32, height: iconSize ?? 32)
                    .padding(.vertical, iconVerticalPadding)
                    .padding(.leading, iconLeadingPadding ?? 3)
                    .padding(.trailing, iconTrailingPadding ?? 13)
            }else{
                AsyncImage(url:URL(string:iconUrl ?? "")!,
                         placeholder: {
                                if(iconDefault != ""){
                                    Image(iconDefault)
                                   .aspectRatio(contentMode: .fit)
                                   .frame(width:iconSize ?? 32, height: iconSize ?? 32)
                                   .padding(.vertical, iconVerticalPadding)
                                   .padding(.leading, iconLeadingPadding ?? 10)
                                   .padding(.trailing, iconTrailingPadding ?? 13).clipShape(Circle())
                                  }
                            },
                               image: { Image(uiImage: $0).resizable() }
                            )
                           .frame(width:iconSize ?? 32, height: iconSize ?? 32)
                           .padding(.vertical, iconVerticalPadding)
                           .padding(.leading, iconLeadingPadding ?? 3)
                           .padding(.trailing, iconTrailingPadding ?? 13)
                           
            }
       
        }.padding(.horizontal, 10)
    }
    
    var leadingView: some View {
        HStack ( spacing: 0){
            iconLeftView
        }
    }
    
    var leadingBackView: some View {
        HStack ( spacing: 0){
            Image(iconLeft).padding(.leading, 10)
        }
    }
    
    var trailingView: some View {
        HStack(){
            if (iconRightCenter != nil && iconRightCenter != "") {
                if let callback = callback {
                      Button(action: {
                          callback("icRCenterClick")
                      }) {
                          Image(iconRightCenter).padding(.leading, 10)
                      }
                  }
                  else {
                      Image(iconRightCenter).padding(.leading, 10)
                  }
            }
            if (iconRight != nil && iconRight != "") {
                if let callback = callback {
                      Button(action: {
                          callback("icRightClick")
                      }) {
                          Image(iconRight).padding(.leading, 10)
                      }
                  }
                  else {
                      Image(iconRight).padding(.leading, 10)
                  }
            }
        }
    }
    
    var navBarTitleLeftView: some View {
        VStack(alignment: .leading){
           Text(title).font(Font.Typography.apply(font: .main, size: .medium)).padding(.bottom, 1).frame(maxWidth: .infinity, alignment: .leading)
            if (subTitle != nil && subTitle != "") {
                Text(subTitle).font(Font.Typography.apply(font: .main, size: .small)).opacity(0.5).frame(maxWidth: .infinity, alignment: .leading)
            }
        }
    }
    
    var navBarTitleCenterView: some View {
           VStack(alignment: .leading){
              Text(title).font(Font.Typography.apply(font: .main, size: .medium)).padding(.bottom, 1)
               if (subTitle != nil && subTitle != "") {
                   Text(subTitle).font(Font.Typography.apply(font: .main, size: .small)).opacity(0.5)
               }
           }
    }
    
    var content: some View {
        WRNavigationBar {
//            ScrollView(.vertical) {
//                VStack(spacing: 0) {
//                    NavigationLink(
//                        destination: destinationView,
//                        label: {
//                            //contentView here
//                        })
//                }
//            }
        }
        .title("")
        .foreground(Color("mainText"))
        .isBackButtonBlack(false)
        .background(Color("white"))
        .navigationBarTitleView(titleView: {
            Group {
              if let callback = callback {
                  Button(action: {
                      callback("navLeftClick")
                  }) {
                      navBarTitleLeftView.foregroundColor(foregrounColor)
                  }
              }
              else {
                  navBarTitleLeftView.foregroundColor(foregroundInactiveColor)
              }
            }
        })
        .wrNavigationBarItems(leading: {
            Group {
                  if let callback = callback {
                      Button(action: {
                          callback("left")
                      }) {
                          if (iconLeft != nil && iconUrl == nil) {
                              leadingBackView.foregroundColor(foregrounColor)
                          }else{
                              leadingView.foregroundColor(foregrounColor)
                          }
                      }
                  }
                  else {
                      leadingBackView
                          .foregroundColor(foregroundInactiveColor)
                  }
            }
        }
        , trailing: {
            trailingView
        }).padding(.top, 10)
    }
    
    public var body: some View {
        Group {
            content
        }
    }
}

struct OmiHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        OmiHeaderView(iconUrl:"https://minio.infra.omicrm.com/crm/5d1d65daa9c6fb0008dd81c2/tenant_logo/2019/08/d39a3954-6b66-431e-a590-d1aabddb6aab.png", iconRight:"icCall", iconRightCenter:"icBack", iconDefault: "defaultCompanyLogo", title: "Chào, admin", subTitle:"vihat"){ event in
                                   print("OmiHeaderView: \(event)")
                               }
//           OmiHeaderView(iconUrl:"https://minio.infra.omicrm.com/crm/5d1d65daa9c6fb0008dd81c2/tenant_logo/2019/08/d39a3954-6b66-431e-a590-d1aabddb6aab.png", iconRight:"icCall", iconRightCenter:"icBack", iconDefault: "defaultCompanyLogo", title: "Chào, admin", subTitle:"vihat"){ event in
//               print("OmiHeaderView: \(event)")
//           }
//           OmiHeaderView(iconUrl: nil, iconLeft: "icBack",  iconDefault: nil, title: "Trở về", subTitle:""){ event in
//               print("OmiHeaderView: \(event)")
//           }
    }
}
