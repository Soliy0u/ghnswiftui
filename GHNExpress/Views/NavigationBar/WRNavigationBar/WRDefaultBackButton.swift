//
//  WRDefaultBackButton.swift
//  OMICall Contact Center
//
//  Created by MacBook on 11/23/21.
//

import Foundation
import SwiftUI

// add @Environment(\.presentationMode) var presentationMode
struct WRDefaultBackButton: View {

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var named: Image?
    var isBlack: Bool = true
    var foreground: Color = .black
    var action: WRBlock?

    init(named: Image? = nil, tapAction: WRBlock? = nil, isBlack: Bool = true, foreground: Color = .black) {
        self.named = named
        self.action = tapAction
        self.isBlack = isBlack
        self.foreground = foreground
    }

    var body: some View {
        Button(action: {
            if let action = action {
                action()
            } else {
                self.presentationMode.wrappedValue.dismiss()
            }
        }) {
            backImage()
                .aspectRatio(contentMode: .fit)
                .foregroundColor(foreground)
        }
    }

    private func backImage() -> Image {
        if named != nil {
            return named!
        } else {
            return Image(uiImage: isBlack ? wrImg(name: "back_arrow") : wrImg(name: "back_arrow_white"))
        }
    }

    private func wrImg(name: String, type: String = "png") -> UIImage {
        return UIImage(named: name, in: Bundle.wrNavigationBarBundle, compatibleWith: nil) ?? UIImage()
    }
}

struct WRDefaultBackButton_Previews: PreviewProvider {
    static var previews: some View {
        WRDefaultBackButton(isBlack: true)
    }
}
