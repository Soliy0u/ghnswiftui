//
//  WRLeadingView.swift
//  OMICall Contact Center
//
//  Created by MacBook on 11/23/21.
//

import Foundation
import SwiftUI

/// navigationBar leading view
struct WRLeadingView: View {

    var content: AnyView?
    
    var body: some View {
        if content != nil {
            content
        } else {
            Color.clear
        }
    }
}
