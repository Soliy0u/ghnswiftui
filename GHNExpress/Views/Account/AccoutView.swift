//
//  AccoutView.swift
//  GHNExpress
//
//  Created by MacBook on 5/31/22.
//

import Foundation
import SwiftUI
import i18nSwift

struct AccountView: View {
    @StateObject var accoutViewModel = AccountViewModel();
    
    var body: some View {
        WRNavigationBar {
            VStack(spacing: 0) {
//                EmptyFunctionCustom()
                info
                Spacer()
            }
        }
        .backButtonHidden(false)
//        .backButtonImageName(Image.icBack)
        .wrNavigationBarItems(leading: {
            Text("Trang cá nhân").modifier(TextModifierCustom(fontType: .bold, fontSize: .medium)).foregroundColor(Color.mainText)
        }, trailing: {
            HStack{
                Text("Đăng xuất").modifier(TextModifierCustom(fontType: .bold, fontSize: .medium)).foregroundColor(Color.mainText)
                Image.icNotifiActive.foregroundColor(Color.mainIcon)
            }
            
        })
    }
    
    /// Render function
    func renderFunction(image: Image?, title: String, havePadding: Bool = true, haveRightIC: Bool = true, onTouch: @escaping () -> Void) -> some View {
        VStack {
            Button(action:
                    onTouch
            ) {
                HStack {
                    if image != nil {
                        image.foregroundColor(.mainText)
                    }
                    Text(title).modifier(TextModifierCustom(fontType: .base, fontColor: .mainText)).padding(.horizontal, image != nil ? .padding16:.padding48)
                    Spacer()
                    if haveRightIC {
                        Image.icChevronsRight
                    }
                } .padding(.horizontal, .padding16)
            }
            Divider().background(Color.bgDivider)
                .padding(.vertical, .padding)
                .padding(.horizontal, havePadding ? .padding16 : 0)
        }
        .padding(.top, .padding8)
        
    }
    
    /// Render info
    var info: some View {
        VStack {
            HStack(spacing: 0){
                VStack{
                    if (accoutViewModel.userInfo.avatar != nil) {
                        AsyncImage(url:URL(string:"avatar")!,
                                   placeholder: {
                            Image.icAccountTab
                                .aspectRatio(contentMode: .fit)
                                .frame(width: .avatarSizeMedium, height: .avatarSizeMedium).clipShape(Circle())
                        },
                                   image: { Image(uiImage: $0).resizable() }
                        )
                            .frame(width: .avatarSizeMedium, height: .avatarSizeMedium)
                    } else {
                        ImageAvatarCustom(name: accoutViewModel.userInfo.fullName ?? "", avatarSize: .avatarSizeMedium)
                    }
                }.padding(.trailing, .padding16)
                VStack(alignment: .leading){
                    HStack{
                        Text(accoutViewModel.userInfo.fullName ?? "PhongNv - 0906797627")
                            .modifier(TextModifierCustom(fontType: .bold))
                        Spacer()
                    }
                    Text(accoutViewModel.userInfo.email ?? "nguyenphong.appcd@gmail.com")
                        .modifier(TextModifierCustom(fontColor: .mainTextLight))
                        .foregroundColor(Color.mainText)
                    Text(accoutViewModel.userInfo._id ?? "ID khách hàng - 3247869")
                        .modifier(TextModifierCustom(fontColor: .orange))
                        .foregroundColor(Color.mainText)
                    Text(accoutViewModel.userInfo.shopId ?? "ID shop - 2937454")
                        .modifier(TextModifierCustom(fontColor: .orange))
                        .foregroundColor(Color.mainText)
                    Text("Phiên bản. - 4.4.9")
                        .modifier(TextModifierCustom(fontColor: .mainTextLight))
                        .foregroundColor(Color.mainText)
                }
                Spacer()
            }
            .padding(.horizontal, .padding16)
           
//            Divider().background(Color.bgDivider)
//                .padding(.vertical, .padding8)
//                .padding(.bottom, .padding8)
            ScrollView(showsIndicators: false) {
                renderFunction(image: Image.icHomeTabActive, title: i18n.t(.storeManage), onTouch: {
    //                accoutViewModel.isAccountEditView = true
                })
                renderFunction(image: Image.icHomeTabActive, title: i18n.t(.rule), onTouch: {
    //                accoutViewModel.isAccountSipView = true
                })
                renderFunction(image: Image.icHomeTabActive, title: i18n.t(.bankInfo), onTouch: {
    //                accoutViewModel.isAccountEditPassView = true
                })
                renderFunction(image: Image.icHomeTabActive, title: i18n.t(.accountInfo), onTouch: {
    //                accoutViewModel.isAccountEditPassView = true
                })
                renderFunction(image: Image.icHomeTabActive, title: i18n.t(.setting), onTouch: {
    //                accoutViewModel.isAccountEditPassView = true
                })
                renderFunction(image: nil, title: i18n.t(.requiredNote), haveRightIC: false, onTouch: {
    //                accoutViewModel.isAccountSettingView = true
                })
                renderFunction(image: nil, title: i18n.t(.optionPayment), haveRightIC: false, onTouch: {
    //                accoutViewModel.isAccountSettingView = true
                })
                renderFunction(image: nil, title: i18n.t(.note), haveRightIC: false, onTouch: {
    //                accoutViewModel.isAccountSettingView = true
                })
                VStack{
    //                logout.keyboardAdaptive()
                    Spacer()
                    Text("Về doanh nghiệp").modifier(TextModifierCustom(fontSize:.small ,fontColor: .mainTextLight))
                    Spacer()
                }
            }
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        ChatView()
    }
}
