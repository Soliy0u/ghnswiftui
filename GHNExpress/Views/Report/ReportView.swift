//
//  ReportView.swift
//  GHNExpress
//
//  Created by MacBook on 5/31/22.
//

import SwiftUI
import i18nSwift

struct ReportView: View {
    @StateObject var reportViewModel = ReportViewModel();
    
    var body: some View {
        WRNavigationBar {
            VStack(spacing: 0) {
               content
                Spacer()
            }
        }
        .backButtonHidden(false)
//        .backButtonImageName(Image.icBack)
        .wrNavigationBarItems(leading: {
            Text("COD & Đối soát").modifier(TextModifierCustom(fontType: .bold, fontSize: .medium)).foregroundColor(Color.mainText)
        }, trailing: {
            Image.icNotifiActive.foregroundColor(Color.mainIcon)
        })
    }
    
    /// Search view
    var searchView: some View{
        HStack(alignment: .center){
            CustomTextfieldSearch(inputTitle: "", username: $reportViewModel.keyword, placeHolder: i18n.t(.search), isSecure: false, iconLeft:"icSearch", onCommit: {
                reportViewModel.onSearch()
            })
        }
        .padding(.horizontal, .padding16)
        .padding(.bottom, .padding8)
    }
    
    /// report tab
    var reportTab: some View{
        VStack(alignment: .center) {
            TabAboveCustom(tabs: .constant(["Đối soát thu hộ", "Comming soon"]),
                           selection: $reportViewModel.selectedContactTab,
                           underlineColor: Color.mainText) { title, isSelected in
                Text(title).modifier(TextModifierCustom(fontType: .bold, fontColor:isSelected ? Color.orange: Color.mainText)).opacity(isSelected ? 1: 0.5)
            } callback: {
                reportViewModel.switchTab()
            }
        }
        .padding(.horizontal, .padding16)
        .padding(.top, .padding16)
    }
    
    var content: some View {
        VStack(alignment: .leading){
            searchView
            reportTab
            ScrollView(showsIndicators: false) { EmptyFunctionCustom()
            }
        }
    }
}

struct ReportPreviews: PreviewProvider {
    static var previews: some View {
        ReportView()
    }
}
