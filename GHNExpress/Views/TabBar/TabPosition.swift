//
//  TabPosition.swift
//  GHNExpress
//
//  Created by MacBook on 5/27/22.
//

import Foundation

/**
 An enum that represents the tab position.
 */
public enum TabPosition: Int {
    /// Tab 1 position.
    case tab1 = 0
    /// Tab 2 position.
    case tab2 = 1
    /// Tab 3 position.
    case tab3 = 2
    /// Tab 4 position.
    case tab4 = 3
    /// Tab 5 position.
    case tab5 = 4
    /// Tab 6 position.
    case tab6 = 5
    /// Tab 7 position.
    case tab7 = 6
}
