//
//  TabBarView.swift
//  GHNExpress
//
//  Created by MacBook on 5/27/22.
//

import SwiftUI//
import Foundation
import SwiftUI
import i18nSwift


struct TabBarView: View {
    @State private var selectedTab = TabPosition.tab1
    @State private var showModal: Bool = false // Show modal
//    @EnvironmentObject var global: GlobalModel // Global info
    
    init() {
        let appearance = UITabBarAppearance()
//        appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 12)!]
//        appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 12)!]
        appearance.backgroundColor = UIColor.white
        appearance.shadowColor = UIColor.white
        UITabBar.appearance().standardAppearance = appearance
    }
    
    /// Render tab item
    func renderTabItem(position: TabPosition, title: String, iconSelected: Image, iconUnSelected: Image, proxy: ScrollViewProxy) -> some View {
        Button(action: {
            if (self.selectedTab == position) {
                withAnimation {
                    proxy.scrollTo(position)
                }
            }
            self.selectedTab = position
        } ) {
            VStack() {
                selectedTab == position ? iconSelected.foregroundColor(.mainText): iconUnSelected.foregroundColor(.mainTextLight)
                Text(title).modifier(TextModifierCustom(fontType: selectedTab == position ? .bold : .main, fontSize: .small, fontColor: selectedTab == position ? .mainText : .mainTextLight))
            }
        }
    }
    
    var body: some View {
        ZStack{
            NavigationView {
                ScrollViewReader { proxy in
                    ZStack(alignment: Alignment.bottom) {
                        TabView(selection: $selectedTab) {
                            HomePageView()
                                .navigationBarTitle("")
                                .navigationBarHidden(true)
                                .tag(TabPosition.tab1)
                            OrderView()
                                .navigationBarTitle("")
                                .navigationBarHidden(true)
                                .tag(TabPosition.tab2)
                            ChatView()
                                .navigationBarTitle("")
                                .navigationBarHidden(true)
                                .tag(TabPosition.tab3)
                            ReportView()
                                .navigationBarTitle("")
                                .navigationBarHidden(true)
                                .tag(TabPosition.tab4)
//                            LoginView()
//                                .navigationBarTitle("")
//                                .navigationBarHidden(true)
//                                .tag(TabPosition.tab5)
                            AccountView()
                                .navigationBarTitle("")
                                .navigationBarHidden(true)
                                .tag(TabPosition.tab6)
                        }
                        HStack(alignment: .center) {
                            renderTabItem(position: TabPosition.tab1, title: i18n.t(.home), iconSelected: Image.icHomeTabActive, iconUnSelected: Image.icHomeTab, proxy: proxy)
                                .frame(maxWidth: .infinity)
                            renderTabItem(position: TabPosition.tab2, title: i18n.t(.order), iconSelected: Image.icOrderTabActive, iconUnSelected: Image.icOrderTab, proxy: proxy)
                                .frame(maxWidth: .infinity)
//                            Button(action: { self.showModal.toggle()} ) {
//                                Image.icCallTab
//                                    .foregroundColor(Color(.white))
//                                    .padding(CGFloat.padding8)
//                                    .background(Color.main)
//                                    .cornerRadius(.cornerRadius12)
//                                    .overlay(RoundedRectangle(cornerRadius: .cornerRadius12).stroke(Color.main, lineWidth: 1))
//                                    .shadow(color: Color.bgShadow, radius: .cornerRadius)
//                            }.frame(maxWidth: .infinity)
                            renderTabItem(position: TabPosition.tab3, title: i18n.t(.chat), iconSelected: Image.icChatTabActive, iconUnSelected: Image.icChatTab, proxy: proxy)
                                .frame(maxWidth: .infinity)
                            renderTabItem(position: TabPosition.tab4, title: i18n.t(.monitor), iconSelected: Image.icMonitorTabActive, iconUnSelected: Image.icMonitorTab, proxy: proxy)
                                .frame(maxWidth: .infinity)
//                            renderTabItem(position: TabPosition.tab5, title: i18n.t(.report), iconSelected: Image.icReportTabActive, iconUnSelected: Image.icReportTab, proxy: proxy)
//                                .frame(maxWidth: .infinity)
                            renderTabItem(position: TabPosition.tab6, title: i18n.t(.account), iconSelected: Image.icAccountTabActive, iconUnSelected: Image.icAccountTab, proxy: proxy)
                                .frame(maxWidth: .infinity)
                        }
                        .padding(.horizontal, .padding16)
                    }
                    .ignoresSafeArea(.keyboard)
                    .accentColor(.mainText)
//                    .overlay(
//                        NavigationLink(destination: MainCallView().environmentObject(global), isActive: $showModal) {
//                            EmptyView()
//                        }
//                    )
                }
            }.navigationViewStyle(StackNavigationViewStyle())
//            ModalView(isShowing: $global.callDetailModal.isShowCallDetail).modalContent(contentModal: {
//                CallLogDetailModal(callLog: $global.callDetailModal.callLog)
//            })
        }
    }
}
