//
//  HomePageView.swift
//  GHNExpress
//
//  Created by MacBook on 5/28/22.
//

import SwiftUI
import i18nSwift
import SwiftfulLoadingIndicators

struct HomePageView: View {
    @StateObject var homePageModel = HomePageViewModel();
    @State var showModal = false;
    
    var body: some View {
        WRNavigationBar {
            VStack(spacing: 0) {
                content
                Spacer()
            }.sheet(isPresented: $showModal) {
//                SPPermissionsList(permissions: [.microphone])
            }
        }
        .backButtonHidden(false)
//        .backButtonImageName(Image.icBack)
        .wrNavigationBarItems(leading: {
            Text("Trang chủ").modifier(TextModifierCustom(fontType: .bold, fontSize: .medium)).foregroundColor(Color.mainText)
        }, trailing: {
            HStack{
                Image.icNotifiActive.foregroundColor(Color.mainIcon)
            }
            
        })
    }
    
    /// Search view
    var searchView: some View{
        HStack(alignment: .center){
            CustomTextfieldSearch(inputTitle: "", username: $homePageModel.keyword, placeHolder: i18n.t(.search), isSecure: false, iconLeft:"icSearch", onCommit: {
                homePageModel.onSearch()
            })
        }
        .padding(.horizontal, .padding16)
        .padding(.bottom, .padding8)
    }
    
    var content: some View {
        VStack(alignment: .leading){
            searchView
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading){
                    HStack{
                        Text("Báo cáo vận hành hôm nay").modifier(TextModifierCustom(fontType: .base, fontSize: .medium))
                        Text(" - live").modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: Color.orange100))
                        LoadingIndicator(animation: .pulse, color: Color.orange100, size: .small, speed: .slow)
                    }
                    Text("Báo cáo trạng thái lấy/giao hàng của các đơn hàng được xử lý ngày hôm nay.").modifier(TextModifierCustom(fontType: .base, fontSize: .small))
                    Text("Cập nhật lúc 15:36 - 28/05/2022").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.orange100)).padding(.top, 5)
                }
                
                VStack(){
                    VStack(alignment: .leading){
                        HStack{
                            VStack(alignment: .leading){
                                Text("Lấy hàng thành công").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                                HStack{
                                    Text("1000").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                
                                Text("Giao hàng thành công").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                                HStack{
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                
                                Text("Hoàn thành thành công").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                                HStack{
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                
                            }.padding(.trailing, 20)
                            
                            VStack(alignment: .leading){
                                Text("Đang giao hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                                HStack{
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                
                                Text("Giao hàng thất bại - lưu kho giữ lại").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                                HStack{
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                Text("Hàng thất lạc - hư hỏng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                                HStack{
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                            }
                        }
                    }.padding(.padding16)
                    HStack{
                        VStack(alignment: .leading){
                            Text("Giao hàng thất bại - Chờ xác nhận giao lại").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).padding(.top, 1)
                            HStack{
                                Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("đơn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 2)
                            
                        }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading).padding(.leading, .padding16).padding(.top, 1).padding(.bottom, .padding)
                    }.background(Color.orange100)

                }.background(Color.main)
                .cornerRadius(.cornerRadius16)
               
                VStack(alignment: .leading){
                    VStack(alignment: .leading){
                        HStack{
                            Text("Báo cáo dòng tiền").modifier(TextModifierCustom(fontType: .base, fontSize: .medium))
                            Text(" - live").modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: Color.orange100))
                            LoadingIndicator(animation: .pulse, color: Color.orange100, size: .small, speed: .slow)
                        }
                        Text("Cập nhật lúc 15:36 - 28/05/2022").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.orange100))
                    }
                    
                    VStack(){
                        VStack(alignment: .leading){
                            Text("Số dư hoàn tất (GHN sắp chuyển cho khách)").modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor: Color.white)).padding(.top, .padding)
                            HStack{
                                HStack{
                                    Text("Tiền thu hộ (COD)").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                    Image.icChevronsRight.foregroundColor(Color.white)
                                }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                              
                            }.padding(.top, 1)
                            HStack{
                                HStack{
                                    Text("Đã thanh toán trước").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                    Image.icChevronsRight.foregroundColor(Color.white)
                                }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                            HStack{
                                HStack{
                                    Text("Khuyến mãi").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                    Image.icChevronsRight.foregroundColor(Color.white)
                                }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                              
                                Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                            HStack{
                                HStack{
                                    Text("Phí dịch vụ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                    Image.icChevronsRight.foregroundColor(Color.white)
                                }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                Text("- 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                            HStack{
                                HStack{
                                    Text("Nợ tồn").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                    Image.icChevronsRight.foregroundColor(Color.white)
                                }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                Text("- 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                            Divider().background(Color.white)
                            HStack{
                                Text("Tổng số dư hoàn tất hiện tại").modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor: Color.white)).frame(maxWidth: .infinity, alignment: .leading)
                                Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 2)
                            HStack{
                                VStack(alignment: .leading){
                                    Text("*Tổng số dư hoàn tất hiện tại = Tiền thu hộ + Đã thanh toán trước + Khuyến mãi + Phí dịch vụ + Nợ tồn").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.orange100))
                                    Text("Bấm vào đây để xem diễn giải chi tiết từng mục").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.mainText)).padding(.top, .padding)
                                    
                                }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading).padding(.leading, .padding16).padding(.top, .padding8).padding(.bottom, .padding8)
                            }.background(Color.white).cornerRadius(.cornerRadius)
                        }.padding(.padding16)
                        
                        VStack(){
                            VStack(alignment: .leading){
                                Text("Số dư đang xử lý (GHN giữ lại)").modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor: Color.white)).padding(.top, .padding)
                                HStack{
                                    HStack{
                                        Text("Số dư qua ví").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                        Image.icChevronsRight.foregroundColor(Color.white)
                                    }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                    Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                HStack{
                                    HStack{
                                        Text("COD hàng lưu/đang xử lý").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                        Image.icChevronsRight.foregroundColor(Color.white)
                                    }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                    Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 1)
                                
                                Divider().background(Color.white)
                                HStack{
                                    Text("Tổng số dư đang xử lý").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).frame(maxWidth: .infinity, alignment: .leading)
                                    Text("+ 0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("vnđ").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                                }.padding(.top, 2)
                                HStack{
                                    VStack(alignment: .leading){
                                        Text("*Tổng số dư đang xử lý = Số dư ví + COD hàng lưu kho/đang xử lý").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.orange100))
                                        Text("Bấm vào đây để xem diễn giải chi tiết từng mục").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.mainText)).padding(.top, .padding)
                                    }.frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading).padding(.leading, .padding16).padding(.top, .padding8).padding(.bottom, .padding8)
                                }.background(Color.white).cornerRadius(.cornerRadius)
                            }.padding(.padding16)
                        }
                    }.background(Color.main)
                    .cornerRadius(.cornerRadius16)
                }
                
                
            }.padding(.leading, .padding16)
             .padding(.trailing, .padding16)
        }
    }
    
}

struct HomePageView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageView()
    }
}
