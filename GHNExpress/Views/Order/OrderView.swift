//
//  OrderView.swift
//  GHNExpress
//
//  Created by MacBook on 5/31/22.
//

import SwiftUI
import i18nSwift

struct OrderView: View {
    @StateObject var orderViewModel = OrderViewModel();
    
    var body: some View {
        WRNavigationBar {
            VStack(spacing: 0) {
                content
                Spacer()
            }
        }
        .backButtonHidden(false)
//        .backButtonImageName(Image.icBack)
        .wrNavigationBarItems(leading: {
            Text("Đơn hàng").modifier(TextModifierCustom(fontType: .bold, fontSize: .medium)).foregroundColor(Color.mainText)
        }, trailing: {
            Image.icNotifiActive.foregroundColor(Color.mainIcon)
        })
    }
    
    /// Search view
    var searchView: some View{
        HStack(alignment: .center){
            CustomTextfieldSearch(inputTitle: "", username: $orderViewModel.keyword, placeHolder: i18n.t(.search), isSecure: false, iconLeft:"icSearch", onCommit: {
                orderViewModel.onSearch()
            })
        }
        .padding(.horizontal, .padding16)
        .padding(.bottom, .padding8)
    }
    
    /// order tab
    var orderTab: some View{
        VStack {
            TabAboveCustom(tabs: .constant(["GHN đã lấy hàng", "GHN chưa lấy hàng", "ĐH nháp"]),
                           selection: $orderViewModel.selectedContactTab,
                           underlineColor: Color.mainText) { title, isSelected in
                Text(title).modifier(TextModifierCustom(fontType: .bold, fontColor:isSelected ? Color.orange: Color.mainText)).opacity(isSelected ? 1: 0.5)
            } callback: {
                orderViewModel.switchTab()
            }
        }
        .padding(.horizontal, .padding16)
        .padding(.top, .padding16)
    }
    
    var content: some View {
        VStack(alignment: .leading){
            searchView
            orderTab
            ScrollView(showsIndicators: false) {
                VStack(){
                    VStack(alignment: .leading){
                        VStack(alignment: .leading){
                            Text("Tổng đơn hàng GHN đã lấy").modifier(TextModifierCustom(fontType: .base, fontSize: .medium, fontColor: Color.white)).padding(.top, .padding8)
                            HStack{
                                Text("Đang xử lý").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                                Divider().background(Color.white)
                            HStack{
                                    Text("Đang giao hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                                Divider().background(Color.white)
                            HStack{
                                    Text("Chờ xác nhận giao lại").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.orange)).frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.orange))
                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.orange))
                                }.padding(.top, 1)
                                    Divider().background(Color.white)
                            HStack{
                                    Text("Đã giao hàng và chưa chuyển khoản").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
                                Divider().background(Color.white)
                            HStack{
                                    Text("Đã đối soát và ck thành công").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white)).frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
                            }.padding(.top, 1)
//                                Divider().background(Color.white)
//                            HStack{
//                                    Text("Đang hoàn hàng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
//                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
//                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
//                            }.padding(.top, 1)
//                                Divider().background(Color.white)
//                            HStack{
//                                    Text("Hoàn hàng thành công").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
//                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
//                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
//                            }.padding(.top, 1)
//                                Divider().background(Color.white)
//                            HStack{
//                                    Text("Hàng thất lạc - hư hỏng").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
//                                    Text("0").modifier(TextModifierCustom(fontType: .bold, fontSize: .large, fontColor: Color.white))
//                                    Text("ĐH").modifier(TextModifierCustom(fontType: .base, fontSize: .small, fontColor: Color.white))
//                            }.padding(.top, 1)
                        }
                    }.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading).padding(.leading, .padding16).padding(.top, 1).padding(.bottom, .padding).padding(.trailing, .padding16)
                }.background(Color.main)
                .cornerRadius(.cornerRadius16)
            }.padding(.leading, .padding16)
                .padding(.trailing, .padding16)
        }
    }
}

struct OrderView_Previews: PreviewProvider {
    static var previews: some View {
        OrderView()
    }
}
