//
//  Images.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import SwiftUI

extension Image {
    static let icGhnLogo = Image("icGhnLogo")
    static let icEyeClosed = Image("icEyeClosed")
    static let icEyeOpened = Image("icEyeOpened")
    static let icNotifiActive = Image("icNotifyActive")
    static let icChevronsRight = Image("icChevronsRight")
    static let icNoData = Image("icNoData")
    // Tab icon
    static let icHomeTab = Image("icHomeTab")
    static let icHomeTabActive = Image("icHomeTabActive")
    static let icOrderTab = Image("icOrderTab")
    static let icOrderTabActive = Image("icOrderTabActive")
    static let icChatTab = Image("icChatTab")
    static let icChatTabActive = Image("icChatTabActive")
    static let icMonitorTab = Image("icMonitorTab")
    static let icMonitorTabActive = Image("icMonitorTabActive")
    static let icReportTab = Image("icReportTab")
    static let icReportTabActive = Image("icReportTabActive")
    static let icAccountTab = Image("icAccountTab")
    static let icAccountTabActive = Image("icAccountTabActive")
}
