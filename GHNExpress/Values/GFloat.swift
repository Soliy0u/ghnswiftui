//
//  GFloat.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//
import Foundation
import SwiftUI
extension CGFloat {
    static let padding = 4.0
    static let margin = 4.0
    static let padding8 = 8.0
    static let margin8 = 8.0
    static let padding12 = 12.0
    static let padding16 = 16.0
    static let margin16 = 16.0
    static let padding24 = 24.0
    static let padding32 = 32.0
    static let padding48 = 48.0
    static let cornerRadius = 6.0
    static let cornerRadius12 = 12.0
    static let cornerRadius16 = 16.0
    static let cornerRadius2 = 2.0
    static let divideHeight = 1.0
    static let avatarSize = UIScreen.main.bounds.width / 10 > 48 ? 48.0 : UIScreen.main.bounds.width / 10
    static let avatarSizeMedium = UIScreen.main.bounds.width / 7.5
    static let avatarSizeLarge = UIScreen.main.bounds.width / 5
    static let maxHeightModal = UIScreen.main.bounds.height / 2
    static let paddingBottomModal = 48.0
    static let minHeightModal = 200.0
}

