//
//  Colors.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//
import Foundation
import SwiftUI

/// Key value from colors in asset
extension Color {
    static let main = Color("main")
    static let mainIcon = Color("mainIcon")
    static let mainText = Color("mainText")
    static let mainText10 = Color("mainText10")
    static let mainTextLight = Color("mainTextLight")
    static let mainTextDark = Color("mainTextDark")
    static let bgInputFocus = Color("bgInputFocus")
    static let bgInput = Color("bgInput")
    static let borderInput = Color("borderInput")
    static let green100 = Color("green100")
    static let green10 = Color("green10")
    static let red100 = Color("red100")
    static let red10 = Color("red10")
    static let gray10 = Color("gray10")
    static let bgDivider = Color("bgDivider")
    static let bgShadow = Color("bgShadow")
    static let bgDark = Color("bgDark")
    static let white10 = Color("white10")
    static let white50 = Color("white50")
    static let orange100 = Color("orange100")
}

