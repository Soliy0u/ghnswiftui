//
//  ImageAvatarCustom.swift
//  OMICall Contact Center
//
//  Created by Tuan on 03/03/2022.
//

import Foundation
import SwiftUI

struct ImageAvatarCustom: View {
    let name: String // Name for avatar
    var avatarSize: CGFloat = .avatarSize // Avatar size
    
    /// Get prefix name
    private func getPrefixName(_ name: String?) -> String {
        var value = ""
        if (name != nil && !name!.isEmpty) {
            var valueName = name!.components(separatedBy: CharacterSet.decimalDigits).joined()
            valueName = valueName.replacingOccurrences(of: "/", with: "")
            let split = valueName.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: " ")
            value = String(split.suffix(1).joined(separator: ""))
        }
        return !value.isEmpty ? value.prefix(1).uppercased() : "A"
    }
    
    /// Get color hex
    private func getColorHex(_ name: String?) -> Color {
        let opacity = 0.5
        let value = self.getPrefixName(name)
        let templateColors = [
            0x1468ee,
            0x32b366,
            0xff5955,
            0xfea220,
            0xa540b8,
            0x57bfdb,
            0xe82a8f,
            0x1e3150,
        ]
        let colorOpts = ["ABCD", "EFGH", "IJK", "LMN", "OPQ", "RST", "UVW", "XYZ"]
        for index in 0...colorOpts.count - 1 {
            if (colorOpts[index].contains(value.prefix(1))) {
//                return Color(hex: templateColors[index], opacity: opacity)
            }
        }
        return Color.mainTextLight
    }
    
    var body: some View {
        ZStack(alignment: .center){
            Circle().frame(width: avatarSize, height: avatarSize).foregroundColor(getColorHex(name))
            Text(!name.isEmpty ? self.getPrefixName(name) : "K").modifier(TextModifierCustom(fontType: .bold, fontColor: .white))
        }
    }
}

