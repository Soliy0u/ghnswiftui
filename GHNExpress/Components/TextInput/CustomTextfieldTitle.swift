//
//  CustomTextfieldTitle.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import SwiftUI
struct CustomTextfieldTitle: View {
    @State var inputTitle: String
    @Binding var content: String
    @Binding var warningContent: String
    @State var rightTitle: String
    @State var placeHolder: String
    @State var isSecure: Bool = false
    @State private var showPassword = true
    @Binding var isFocused: Bool?
    var bgInput: Color = Color.bgInput // Background input
    var bgInputFocus: Color = Color.bgInputFocus // Background input focus
    var bgBorder: Color = Color.borderInput // Border background
    var bgBorderFocus: Color = Color.main // Bg border focus
    var titleTextColor: Color = Color.mainText // Title text color
    var textColor: Color = Color.mainText // Text color
    
    /// Title
    var title: some View {
        Text(warningContent.isEmpty ? inputTitle: warningContent).modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor: !warningContent.isEmpty ? .red : titleTextColor))
    }
    
    /// warning Title
    var warningTitle: some View {
        Text(warningContent.isEmpty ? "": warningContent).modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor: !warningContent.isEmpty ? .red : titleTextColor))
    }
    
    /// right Title
    var rightTitleView: some View {
        Text(rightTitle).modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor: titleTextColor))
    }
    
    var body: some View {
        VStack(alignment: .leading){
            HStack{
                title
                rightTitleView.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
            }
            Group{
                if !isSecure {
                    
                    VStack(alignment: .leading, spacing: .padding8){
                        CustomTextfield(placeholder: placeHolder, //placeholder: Text(placeHolder),
                                        fontType: .main,
                                        fontSize: .medium,
                                        fontColor: textColor,
                                        isSecure: false, content: $content,
                                        onEditingChanged: { _isFocused in
                            isFocused = _isFocused;
                        },
                                        onCommit:{
                            print("CInput commit")
                        })
                        
                    }.foregroundColor(isFocused != nil && isFocused!  ? bgInputFocus : bgInput)
                } else {
                    VStack(alignment: .leading, spacing: .padding8){
                        HStack {
                            CustomTextfield(placeholder: placeHolder,//placeholder: Text(placeHolder),
                                            fontType: .main,
                                            fontSize: .medium,
                                            fontColor: textColor,
                                            isSecure: showPassword, content: $content,
                                            onEditingChanged: { _isFocused in
                                isFocused = _isFocused;
                            },
                                            onCommit:{
                                print("CInput commit")
                            })
                            Button(action: {
                                isFocused = false;
                                self.showPassword.toggle()
                            }) {
                                if(showPassword){
                                    Image.icEyeClosed.foregroundColor(Color.mainText)
                                } else{
                                    Image.icEyeOpened.foregroundColor(Color.mainText)
                                }
                                
                            }
                        }
                        
                    }.foregroundColor(isFocused != nil && isFocused!  ? bgInputFocus : bgInput)
                }
            }
            .padding(.vertical, .padding12)
            .padding(.horizontal, .padding16)
            .background(isFocused != nil && isFocused! ? bgInputFocus : bgInput)
            .cornerRadius(.cornerRadius16)
            .foregroundColor(isFocused != nil && isFocused! ? bgInputFocus : bgInput)
            .overlay(RoundedRectangle(cornerRadius: .cornerRadius16).stroke(isFocused != nil && isFocused! ? bgBorderFocus : bgBorder, lineWidth: .divideHeight))
            warningTitle
        }
    }
}

