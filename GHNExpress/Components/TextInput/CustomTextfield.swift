//
//  CustomTextfield.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import UIKit
import SwiftUI
struct CustomTextfield: View {
    var placeholder: String? = ""
    var fontType: AppTypography.FontFamilyTheme = .main
    var fontSize: AppTypography.FontSizeTheme = .medium
    var fontColor: Color = .mainText
    var isSecure: Bool = false
    var iconLeft: String? = nil
    @Binding var content: String
    var onEditingChanged: (Bool)->() = { _ in}
    var onCommit: ()->() = { }
    var onTapGesture: ()->() = { }
    
    var body: some View {
        ZStack(alignment: .leading) {
            HStack{
                if(iconLeft != nil && iconLeft != ""){
                    Image(iconLeft ?? "")
                }
                if(isSecure){
                    ZStack() {
                        SecureField(placeholder ?? "", text: $content).modifier(TextModifierCustom(fontColor: fontColor))
                    }
                } else{
                    TextField(placeholder ?? "", text: $content, onEditingChanged: onEditingChanged, onCommit: onCommit)
                        .modifier(TextModifierCustom(fontColor: fontColor))
                        .autocapitalization(.none)
                }
            }
        }
    }
}
