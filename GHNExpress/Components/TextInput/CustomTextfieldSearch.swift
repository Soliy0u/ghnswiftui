//
//  SFInputComponent.swift
//  OMICall Contact Center
//
//  Created by MacBook on 11/15/21.
//

import Foundation
import SwiftUI
struct CustomTextfieldSearch: View {
    //    MARK:- PROPERTIES
    @State var inputTitle: String
    @Binding var username: String
    @State var placeHolder: String
    @State var isSecure: Bool
    @State var iconLeft: String?
    @State private var isFocused = false;
    var onEditingChanged: (Bool)->() = { _ in} // Edit change vent
    var onCommit: ()->() = { } // Commit event
    var onTapGesture: ()->() = { } // Tap event
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8){
            Text(inputTitle)
                .modifier(TextModifierCustom(fontType: .bold, fontSize: .small, fontColor:  Color.mainText))
            Group{
                if !isSecure {
                    CustomTextfield(placeholder: placeHolder, //Text(placeHolder)
                                    fontType: .main,
                                    fontSize: .medium,
                                    fontColor: Color("mainText"),
                                    isSecure: false,
                                    iconLeft: iconLeft,
                                    content: $username,
                                    onEditingChanged: { _isFocused in isFocused = _isFocused },
                                    onCommit: onCommit)
                        .padding(.horizontal, .padding16)
                        .padding(.vertical, .padding8)
                    
                } else {
                    CustomTextfield(placeholder: placeHolder,//placeholder: Text(placeHolder)
                                    fontType: .main,
                                    fontSize: .medium,
                                    fontColor: Color("mainText"),
                                    isSecure: false,
                                    iconLeft: iconLeft,
                                    content: $username,
                                    onEditingChanged: { _isFocused in isFocused = _isFocused },
                                    onCommit: onCommit)
                }
            }
            .background(isFocused ? Color("bgInputFocus") : Color("bgInput")).cornerRadius(.cornerRadius16)
            .foregroundColor(isFocused ? Color("bgInputFocus") : Color("bgInput"))
            .overlay(RoundedRectangle(cornerRadius: .cornerRadius16).stroke(isFocused ? Color("main") : Color("borderInput"), lineWidth: isFocused ? 1 : 0.5))
        }
    }
}
