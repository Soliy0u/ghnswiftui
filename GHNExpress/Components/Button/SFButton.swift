//
//  SFButton.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import SwiftUI

struct SFButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity)
            .padding(.vertical, .padding12)
    }
}

struct WButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(width: .infinity)
            .padding(.vertical, .padding12)
            .padding(.horizontal, .padding8)

    }
}
