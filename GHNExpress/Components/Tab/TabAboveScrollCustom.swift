//
//  TabAboveScrollCustom.swift
//  OMICall Contact Center
//
//  Created by Tuan on 04/03/2022.
//
import Foundation
import SwiftUI

struct TabAboveScrollCustom: View {
    @Binding var selection: Int // Selection index
    @Binding var tabs: [String] // Tabs
    private let leftOffset: CGFloat = 0.1
    let underlineColor: Color = Color.mainText // Color of the underline of the selected tab
    
    var body: some View {
        ScrollViewReader { proxy in
            VStack(){
                HStack(alignment: .center) {
                    ForEach(tabs.indices) {id in
                        VStack {
                            Spacer()
                            let title = Text(tabs[id]).id(id)
                                .onTapGesture {
                                    withAnimation() {
                                        selection = id
                                    }
                                }
                            if self.selection == id {
                                title.opacity(1)
                            } else {
                                title.opacity(0.5)
                            }
                            Rectangle() // The line under the tab
                                .frame(height: .padding, alignment: .leading)
                                .foregroundColor(self.selection == id  ? underlineColor : .clear)
                                .cornerRadius(.cornerRadius)
                                .transition(.move(edge: .bottom))
                        }
                        .padding(.horizontal, .padding8)
                        .fixedSize(horizontal: true, vertical: false)
                    }
                    .modifier(TextModifierCustom(fontType: .bold, fontColor: .mainText))
                    .padding(.top, .padding)
                }
                .padding(.horizontal, .padding16)
            }
            .onChange(of: selection) { value in
                withAnimation() {
                    proxy.scrollTo(value, anchor: UnitPoint(x: UnitPoint.leading.x + leftOffset, y: UnitPoint.leading.y))
                }
            }.animation(.easeInOut)
        }
    }
}

