//
//  OmiTabView.swift
//  OMICall Contact Center
//
//  Created by MacBook on 12/1/21.
//

import Foundation
import SwiftUI

struct TabAboveCustom<Label: View>: View {
    @Binding var tabs: [String] // The tab titles
    @Binding var selection: Int // Currently selected tab
    let underlineColor: Color // Color of the underline of the selected tab
    // Tab label rendering closure - provides the current title and if it's the currently selected tab
    let label: (String, Bool) -> Label // Label of tab
    let callback: (() -> ())? // Call back function when selected tab
    @State private var scrollViewContentSize: CGSize = .zero
    
    var body: some View {
        // Pack the tabs horizontally and allow them to be scrolled
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .center, spacing: .padding24) {
                ForEach(tabs, id: \.self) {
                    self.renderTab(title: $0)
                }
            }.background(
                GeometryReader { geo -> Color in
                    DispatchQueue.main.async {
                        scrollViewContentSize = geo.size
                    }
                    return Color.clear
                }
            )
        }
        .frame(
            maxWidth: scrollViewContentSize.width
        )
       
    }
    
    /// Render tab
    private func renderTab(title: String) -> some View {
        let index = self.tabs.firstIndex(of: title)!
        let isSelected = index == selection
        
        return Button(action: {
            // Allows for animated transitions of the underline,
            // as well as other views on the same screen
            withAnimation {
                self.selection = index
                if let callback = callback {
                    callback()
                }
            }
        }) {
            VStack(alignment: .leading){
                label(title, isSelected)
                Rectangle() // The line under the tab
                    .frame(width: .infinity, height: 2, alignment: .leading)
                // The underline is visible only for the currently selected tab
                    .foregroundColor(isSelected ? Color.orange : .clear)
                    .cornerRadius(.cornerRadius)
                    .padding(.top, .padding)
                    .transition(.move(edge: .bottom))
            }
        }
    }
}
