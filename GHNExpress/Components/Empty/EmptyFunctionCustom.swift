//
//  EmptyFunctionCustom.swift
//  GHNExpress
//
//  Created by MacBook on 5/31/22.
//

import Foundation
import SwiftUI
import i18nSwift

struct EmptyFunctionCustom : View {

  var body: some View {
      VStack(alignment: .center){
          Image.icNoData.padding()
          Text(i18n.t(.functionDeveloping)).modifier(TextModifierCustom(fontType: .bold,fontColor: .mainText))
      }
  }
}
