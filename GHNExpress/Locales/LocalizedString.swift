//
//  LocalizedString.swift
//  GHNExpress
//
//  Created by MacBook on 5/27/22.
//

import Foundation
import i18nSwift

/// Define locale string
extension i18n.LocalizedString {
    static let login: i18n.LocalizedString = "login"
    static let search: i18n.LocalizedString = "search"
    static let functionDeveloping: i18n.LocalizedString = "functionDeveloping"
    static let storeManage: i18n.LocalizedString = "storeManage"
    static let rule: i18n.LocalizedString = "rule"
    static let bankInfo: i18n.LocalizedString = "bankInfo"
    static let accountInfo: i18n.LocalizedString = "accountInfo"
    static let setting: i18n.LocalizedString = "setting"
    static let requiredNote: i18n.LocalizedString = "requiredNote"
    static let note: i18n.LocalizedString = "note"
    static let optionPayment: i18n.LocalizedString = "optionPayment"
  
    
    // GHN tabbar
    static let home: i18n.LocalizedString = "home"
    static let order: i18n.LocalizedString = "order"
    static let chat: i18n.LocalizedString = "chat"
    static let monitor: i18n.LocalizedString = "monitor"
    static let report: i18n.LocalizedString = "report"
    static let account: i18n.LocalizedString = "account"
}
