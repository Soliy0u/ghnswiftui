//
//  BaseViewModel.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation

class BaseViewModel: ObservableObject{
    @Published var isShowMessage = false // Varabile show message or not
    @Published var message = "Lỗi tải" // Message toast
    @Published var isLoading = false // Is loading
    @Published var isLoadMore = false // Is load more
    @Published var isRefresh = false // Is refresh
//    var refreshComplete: RefreshComplete? { willSet { self.isRefresh = true }}
    @Published var isShowNoData = false // Is show no data
    
    /// Handle error
    func handleError(error: Error?) {
        self.message = "Lỗi tải"
        self.isShowMessage = true
    }
}
