//
//  StorageValueType.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
enum StorageValueType {
    static let accessToken = "accessToken" // Access token
    static let refreshToken = "refreshToken" // Refresh token
}
