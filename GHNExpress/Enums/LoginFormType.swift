//
//  LoginFormType.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import Foundation
import Focuser

enum LoginFormType {
    case username, password
}

extension LoginFormType: FocusStateCompliant {
    static var last: LoginFormType {
        .password
    }
    
    var next: LoginFormType? {
        switch self {
        case .username:
            return .password
        default: return nil
        }
    }
}
