//
//  AgentModel.swift
//  GHNExpress
//
//  Created by MacBook on 5/31/22.
//

import Foundation

struct AgentModel : Codable, Hashable {
    var _id: String? // Id
    var shopId: String? //Shop Id
    var fullName: String? // Name
    var note: String? // Note
    var email: String? // Email
    var avatar: String? // Avatar
    var isActive: Bool? // Is Active
}
