//
//  GHNExpressApp.swift
//  GHNExpress
//
//  Created by MacBook on 5/26/22.
//

import SwiftUI

@main
struct GHNExpressApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
